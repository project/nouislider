# noUiSlider

[noUiSlider](https://refreshless.com/nouislider/) is a lightweight, ARIA-accessible JavaScript range slider with multi-touch and keyboard support.

This module includes 2 different Drupal libraries that provide noUiSlider for any themes
and modules that require it:
- `nouislider/nouislider` (packaged with module)
- `nouislider/nouislider-cdn` (CDN-hosted version)
